 <div class="row">
        <div class="col-lg-4">
          <img class="rounded-circle" src="{{ URL::asset('/image/blood1.jpg')}}" alt="Generic placeholder image" width="140" height="140">
          <h2>Why should we donate blood?</h2>
          <p>Blood is the most precious gift that anyone can give to another person — the gift of life</p>
          <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="rounded-circle" src="http://i2.mirror.co.uk/incoming/article5849484.ece/ALTERNATES/s615/GettyImages-117452503.png" alt="Generic placeholder image" width="140" height="140">
          <h2>Who all can donate blood? </h2>
          <p>To ensure the safety of blood donation, check for the eligibility below</p>
          <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
          <img class="rounded-circle" src="{{ URL::asset('/image/blood.png')}}" alt="Generic placeholder image"width="140" height="140">
          <h2>how can we donate blood?</h2>
          <p>Be a part of noble deed.Become a part of large blood donar network</p>
          <p><a class="btn btn-secondary" href="#" role="button">View details &raquo;</a></p>
        </div><!-- /.col-lg-4 -->
      </div><!-- /.row -->