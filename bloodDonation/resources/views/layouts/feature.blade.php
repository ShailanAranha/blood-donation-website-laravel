
      <hr class="featurette-divider">

      <div class="row featurette" >
        <div class="col-md-7">
          <h2 class="featurette-heading">OUR MISSION <span class="text-muted"></span></h2>
          <p class="lead">To be the premier provider of blood transfusion practices by delivering innovative industry leading solutions that ensure the highest quality products and services, operating efficiencies, donor satisfaction and improved patient care, resulting in the achievement of our Mission.</p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-fluid mx-auto" src="{{ URL::asset('/image/blood2.jpg')}}" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7 push-md-5">
          <h2 class="featurette-heading">VISION <span class="text-muted"></span></h2>
          <p class="lead">No patient should die because of unavailability of blood.</p>
        </div>
        <div class="col-md-5 pull-md-7">
          <img class="featurette-image img-fluid mx-auto" src="http://www.findingdulcinea.com/docroot/dulcinea/fd_images/news/health/May-June-08/Gay-Activists-Say-Blood-Donation-Restrictions-Are-Outdated/news/0/image.jpg" alt="Generic placeholder image">
        </div>
      </div>

      <hr class="featurette-divider">

      <div class="row featurette">
        <div class="col-md-7">
          <h2 class="featurette-heading">PLEDGE <span class="text-muted"></span></h2>
          <p class="lead">If every healthy individual adult pledges to donate blood every 3 months, we soon will be able to provide blood to all those in need. Every drop of blood is life saving and precious. </p>
        </div>
        <div class="col-md-5">
          <img class="featurette-image img-fluid mx-auto" src="http://images.stockunlimited.net/preview1300/blood-donation-campaign-design_1962085.jpg" alt="Generic placeholder image">
        </div>
      </div>

      <!-- <hr class="featurette-divider"> -->