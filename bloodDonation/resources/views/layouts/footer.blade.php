<footer class="footer-distributed">

			<div class="footer-left">

				<h3>Red<span>line</span></h3>

				<p class="footer-links">
					<a href="#">Home</a>
					·
					<a href="#">donate</a>
					·
					<a href="#">order</a>
					·
					<a href="#">blog</a>
					·
					<a href="#">About us</a>
					·
					<a href="#">Contact us</a>
				</p>

				<p class="footer-company-name">Red-line &copy; 2017</p>
			</div>

			<div class="footer-center">

				<div>
					<i class="fa fa-map-marker"></i>
					<p><span>sector 21</span> wall street, mumbai</p>
				</div>

				<div>
					<i class="fa fa-phone"></i>
					<p>+91 8652314803</p>
				</div>

				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:support@company.com">help@redline.com</a></p>
				</div>

			</div>

			<div class="footer-right">

				<p class="footer-company-about">
					<span>About the company</span>
					We serve the noble cause of supplying good quality blood to the ones in need. We mainly focus on acceptance, preservation and supplying of blood as per demand from the patients.
				</p>


  				<!--
				<div class="footer-icons">

					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-github"></i></a>

				</div>
                --> 
			</div>

		</footer>