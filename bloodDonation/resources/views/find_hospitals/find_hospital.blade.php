
<!DOCTYPE html>
<html>
  <head>
   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">


    <meta charset='utf-8'>
    <title>Where am I?</title>
    <script type='text/javascript'
    src='https://maps.googleapis.com/maps/api/js?sensor=true&libraries=places,weather'></script>
    <script src='/js/myLoc.js'></script>
    <style type='text/css'>
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map-canvas { height: 100% }
    </style>
  </head>
  <body>
  @include('layouts.nav')
  <br> <br><br><br>
    <div id='location'>
      Your location will go here.
    </div>
    <div id='map-canvas'>
      Google Map will go here!.
    </div>
  </body>
</html>