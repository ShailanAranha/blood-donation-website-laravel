<!--form format
name
address
blood group
quantity
phone number
email
hospital phone number
date of requirement
prescription 
payment
-->

<!DOCTYPE html>
<html>
<head>
  <title>Order form</title>


  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<!--
  <link href="/css/donate.css" rel="stylesheet">
  <link href="/js/donate.js" rel="stylesheet">
  <script language="JavaScript" src="/js/donate.js"></script>
  -->

  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">
    <link href="/css/footer1.css" rel="stylesheet">
    <link href="/css/footer2.css" rel="stylesheet">
</head>
<body>
@include('layouts.nav')
<br><br>
<div class="container">

    <form class="well form-horizontal" action="/order" method="post"  id="contact_form">
     {{ csrf_field() }}

    @if ($flash= session('message'))
    <div class="alert alert-success" role="alert">
    {{$flash}}
     </div>
    @endif
    
<fieldset>

<!-- Form Name -->
<legend>Order form</legend>

<!-- Text input-->

<div class="form-group">
  <label class="col-md-4 control-label">First Name</label>  
  <div class="col-md-4 inputGroupContainer">
  <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input  name="first_name" placeholder="First Name" class="form-control"  type="text">
    </div>
  </div>
</div>

<!-- Text input-->

<div class="form-group">
  <label class="col-md-4 control-label" >Last Name</label> 
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
  <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
  <input name="last_name" placeholder="Last Name" class="form-control"  type="text">
    </div>
  </div>
</div>

<!-- Text input-->
       <div class="form-group">
  <label class="col-md-4 control-label">E-Mail</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
  <input name="email" placeholder="E-Mail Address" class="form-control"  type="text">
  <br>
  
    </div>
  </div>
   @if ($errors->has('email'))
  
  <span class="help-block">
  
   <strong>{{ $errors->first('email') }}</strong>
  </span>
  @endif
</div>


<!-- Text input-->
       
<div class="form-group">
  <label class="col-md-4 control-label">Phone #</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
  <input name="phone" placeholder="(845)555-1212" class="form-control" type="text">
    </div>
  </div>
</div>

<!-- Text input-->
<div class="form-group"> 
  <label class="col-md-4 control-label">Blood Group</label>
    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
    <select name="bloodgroup" class="form-control selectpicker" >
      <option value=" " >Choose your blood group</option>
      <option>A-</option>
      <option>A+</option>
      <option >B-</option>
      <option >B+</option>
      <option >AB-</option>
      <option >AB+</option>
      <option >O</option>
      <option >O-</option>
    
    </select>
  </div>
</div>
</div>


<!-- Text input-->
<div class="form-group"> 
  <label class="col-md-4 control-label">Quantity(bottle)</label>
    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
    <select name="quantity" class="form-control selectpicker" >
      <option value=" " >1 bottle=300ml</option>
      <option>1</option>
      <option>2</option>
    
    </select>
  </div>
</div>
</div>



<!-- Text input-->
      
<div class="form-group">
  <label class="col-md-4 control-label">Delivery Address</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
  <input name="deliveryaddress" placeholder="hospital address" class="form-control" type="text">
    </div>
  </div>
</div>

<!-- Text input-->
 
<div class="form-group">
  <label class="col-md-4 control-label">Hospital Phone</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
  <input name="hospitalphone" placeholder="(845)555-1212" class="form-control" type="text">
    </div>
  </div>
</div>



<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label">Date of requirement</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-home"></i></span>
  <input name="dateofrequirement" placeholder="DD/MM/YYYY" class="form-control"  type="text">
    </div>
  </div>
</div>

<!-- Text input-->
      
<div class="form-group">
  <label class="col-md-4 control-label">Doctor's prescription</label>  
    <div class="col-md-4 inputGroupContainer">
    <div class="input-group">
       <input type="file" name="prescription" accept="image/*">
 
    </div>
  </div>
</div>

<!-- Text input-->
<div class="form-group"> 
  <label class="col-md-4 control-label">Payment</label>
    <div class="col-md-4 selectContainer">
    <div class="input-group">
        <span class="input-group-addon"><i class="glyphicon glyphicon-list"></i></span>
    <select name="payment" class="form-control selectpicker" >
      <option value=" " >choose</option>
      <option>1</option>
      <option>2</option>
    </select>
  </div>
</div>
   <p>
      Note<br>

1. Pay(COD):- 1 bottle(300ml)=Rs.1000<br>

2.Donate: - two people on behalf have to donate blood within 3 months period(recipient have to sign the undertaking on delivery)

    </p>
</div>





<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label"></label>
  <div class="col-md-4">
    <button type="submit" class="btn btn-warning" >Send <span class="glyphicon glyphicon-send"></span></button>
  </div>
</div>

</fieldset>
</form>
</div>
    </div><!-- /.container -->


    @include('layouts.footer')
</body>
</html>