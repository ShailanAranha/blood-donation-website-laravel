<!DOCTYPE html>
<html>
<head>
	<title>admin page</title>


   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">

    <style>
table, td, th {
    border: 1px solid black;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th {
    height: 50px;
}
</style>
</head>
<body>
  @include('admin.nav')
     <script src="{{ asset('js/app.js') }}"></script>
<br><br><br>
<table class="table table-striped table-inverse">
  <tr>
    <th>Name</th>
    <th>Email</th>
    <th>Password</th>
  </tr>

 @foreach($users as $user) 
  <tr>
    <td>{{ $user->name }}</td>
    <td>{{ $user->email }}</td>
    <td>{{ $user->password }}</td>
  </tr>
@endforeach  
  
</table>

</body>
</html>