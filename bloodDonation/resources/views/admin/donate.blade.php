@include('admin.nav')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">

<style>
table, td, th {
    border: 1px solid black;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th {
    height: 50px;
}
</style>

<br><br>
<h1 style="text-align: center;">Donate List</h1>
<br><br>
<table class="table table-striped table-inverse">
  <tr>
    <th>Firstname</th>
    <th>Latname</th>
    <th>email</th>
    <th>Phone</th>
    <th>address</th>
    <th>city</th>
    <th>state</th>
    <th>age</th>
    <th>gender</th>
    <th>Blood group</th>
    <th>Choose Date</th>
  </tr>

 @foreach($donates as $donate) 
  <tr>
    <td>{{ $donate->first_name }}</td>
    <td>{{ $donate->last_name }}</td>
    <td>{{ $donate->email }}</td>
    <td>{{ $donate->phone }}</td>
    <td>{{ $donate->address }}</td>
    <td>{{ $donate->city }}</td>
    <td>{{ $donate->state }}</td>
    <td>{{ $donate->age }}</td>
    <td>{{ $donate->gender }}</td>
    <td>{{ $donate->bloodgroup }}</td>
    <td>{{ $donate->choosedate }}</td>
  </tr>
@endforeach  
  
</table>