<nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button> <!--<img src="storage/app/public/images/redline logo.png"</img>  src="{{ asset("storage/app/public/images/logo.png") }}"-->
     <!--
     <a class="navbar-brand" href="#"><image img src="{{ URL::asset('/image/logo.png')}}" alt=""></image></a>-->
     <!--
     <a class="navbar-brand" href="#"><img src="{{ URL::asset('/image/logo.png')}}"></a>
      -->
      <a class="navbar-brand" href="#"><img src="{{ URL::asset('/image/logo.png')}}" alt="logo" height="50" width="50"></a>


      <div class="collapse navbar-collapse" id="navbarCollapse">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="/admin">Home <span class="sr-only">(current)</span></a>
          </li>
          
          <li class="nav-item">
            <a class="nav-link" href="/admin/donate">Donate</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="/admin/order">Order</a>
          </li>
          

          <!-- 
          <li class="nav-item">
            <a class="nav-link " href="/blog">Blog</a>
          </li>



          <li class="nav-item">
            <a class="nav-link" href="/about">About us</a>
          </li>
         -->
          <li class="nav-item">
            <a class="nav-link" href="/admin/contact">Contact</a>
          </li>
         <!--
          <li class="nav-item">
            <a class="nav-link" href="/find_hospitals">Find Hospitals</a>
          </li>
          -->
        </ul>



        <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                        <!--
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                            -->
            <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">Login</a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">Register</a>
          </li>

                        @else
                             <li class="nav-item">
             <a href="#" class="nav-link" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }}(Admin) <span class="caret"></span>
                                </a>
          </li>

          <li class="nav-item">
             <a class="nav-link" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
          </li>
                            <!--
                            <li class="nav-item">
                                <a href="#" class="nav-link" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                           --> 
                        @endif
                    </ul>
      </div>
    </nav>