@include('admin.nav')

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">

     <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="/css/app.css" rel="stylesheet">

<style>
table, td, th {
    border: 1px solid black;
}

table {
    border-collapse: collapse;
    width: 100%;
}

th {
    height: 50px;
}
</style>

<br><br>
<h1 style="text-align: center;">Order List</h1>
<br><br>
<table class="table table-striped table-inverse">
  <tr>
    <th>Firstname</th>
    <th>Latname</th>
    <th>email</th>
    <th>Phone</th>
    <th>Blood Group</th>
    <th>Quantity</th>
    <th>Delivery</th>
    <th>Hospital Phone</th>
    <th>Requirement Date</th>
    <th>Prescription</th>
    <th>payment</th>
  </tr>

 @foreach($orders as $order) 
  <tr>
    <td>{{ $order->first_name }}</td>
    <td>{{ $order->last_name }}</td>
    <td>{{ $order->email }}</td>
    <td>{{ $order->phone }}</td>
    <td>{{ $order->bloodgroup }}</td>
    <td>{{ $order->quantity }}</td>
    <td>{{ $order->deliveryaddress }}</td>
    <td>{{ $order->hospitalphone }}</td>
    <td>{{ $order->dateofrequirement }}</td>
    <td>{{ $order->prescription }}</td>
     <td>{{ $order->payment }}</td>
      

  </tr>
@endforeach  
  
</table>