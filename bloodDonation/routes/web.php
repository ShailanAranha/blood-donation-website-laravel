
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//use App\Donar;

Route::get('/', function () {
    return view('layouts.layout');

});

Route::get('/blog', function () {
    return view('blogs.blog');

});



Route::get('/find_hospitals', function(){
	return view('find_hospitals.find_hospital');
});

Route::get('/about', function(){
	return view('layouts.about');
});



Route::get('/donate','DonateController@create');
Route::post('/donate','DonateController@store');
Route::get('/order','OrderController@create');
Route::post('/order','OrderController@store');
Route::get('/contact', 'ContactController@create');
Route::post('/contact', 'ContactController@store');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/admin/login', 'Auth\AdminLoginController@showLoginForm')->name('admin.login');
Route::post('/admin/login', 'Auth\AdminLoginController@login')->name('admin.login.submit');

Route::get('/admin', 'AdminController@index')->name('admin');

Route::get('/admin/donate', 'AdminController@donate');

Route::get('/admin/order', 'AdminController@order');
Route::get('/admin/contact', 'AdminController@contact');
