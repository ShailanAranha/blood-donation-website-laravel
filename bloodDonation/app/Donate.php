<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donate extends Model
{
    protected $fillable=['first_name','last_name','email','phone', 'address', 'city', 'state', 'age', 'gender', 'bloodgroup', 'choosedate'];
}
