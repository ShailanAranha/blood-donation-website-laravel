<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;

class ContactController extends Controller
{
    public function create()
    {
    	return view('contacts.contact');
    }


    public function store()
    {

     //validate the form
    $this->validate(request(),['name'=>'required', 'email'=>'required|email', 'subject'=>'required' , 'message'=>'required'



    	]);
    

     //create and request the user
    $contact=Contact::create(request(['name', 'email', 'subject', 'message']));      

     //flash message (success message)
    session()->flash('message', 'submitted successfully!');

     //redirect the user 

     return redirect('/contact');   
    }
}
