<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Donate;

class DonateController extends Controller
{
    public function create()
    {
    	 return view('donars.donar');

    }


    public function store()
    {

       // return session('message');
    //validate the form

    	$this->validate(request(), [
         'first_name'=>'required',
         'last_name'=>'required',
         'email'=>'required|email',
         'phone'=>'required',
         'address'=>'required',
         'city'=>'required',
         'state'=>'required',
         'age'=>'required',
         'gender'=>'required',
         'bloodgroup'=>'required',
         'choosedate'=>'required'
    		]);
       

    //create and save the user
    
    $donar= Donate::create(request(['first_name','last_name','email','phone', 'address', 'city', 'state', 'age', 'gender', 'bloodgroup', 'choosedate']));


    //displaying flash messaging

   session()->flash('message', 'submitted successfully! Thanks for being a part of noble deed!');


    //redirect to home page with a pop up	
    return redirect('/donate');

    }
}
