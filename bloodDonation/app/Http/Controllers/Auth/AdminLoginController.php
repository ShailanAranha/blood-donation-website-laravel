<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  Illuminate\Support\Facades\Auth;
use App\Admin;
class AdminLoginController extends Controller
{

	public function __construct()
	{
		$this->middleware('guest:admin');
	}
    public function showLoginForm()
    {
    	return view('auth.admin-login');
    }

    public function login(Request $request)
    {
       //validate the form
       $this->validate($request, [
        'email' => 'required|email',
        'password' => 'required|min:6'   
       	]);


    	//attempt to login

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember))
        {
        //if successful, then redirect to the intended loction
        	return redirect()->intended(route('admin'));
        }
    	

    	//if unsuccessful, then redirect back to login page

    	return redirect()->back()->withInput($request->only('email','remember'));
    }
}
