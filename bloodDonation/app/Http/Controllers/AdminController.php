<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Donate;
use App\Order;
Use App\Contact;
class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
    	$users= User::all();
        return view('admin', compact('users'));
    }

    public function donate()
    {
        $donates = Donate::all();

        return view('/admin/donate', compact('donates'));
    }

    public function order()
    {
        $orders = Order::all();
        return view('admin.order', compact('orders'));
    }

    public function contact()
    {
        $contacts= Contact::all();
        return view('admin.contact', compact('contacts'));
    }
}
