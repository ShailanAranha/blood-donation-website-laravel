<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
class OrderController extends Controller
{
    public function create()
    {
    	return view('orders.order');
    }


    public function store()
    {
    	//validate the form
    	$this->validate(request(), [
    		'first_name'=>'required',
    		'last_name'=>'required',
    		'email'=>'required|email',
    		'phone'=>'required',
    		'bloodgroup'=>'required',
    		'quantity'=>'required',
    		'deliveryaddress'=>'required',
    		'hospitalphone'=>'required',
    		'dateofrequirement'=>'required',
    		'prescription'=>'required',
    		'payment'=>'required'
       		]);


    	//create and save the user

    	$order= Order::create(request(['first_name', 'last_name', 'email', 'phone', 'bloodgroup', 'quantity', 'deliveryaddress', 'hospitalphone', 'dateofrequirement', 'prescription', 'payment']));


    	//flash message

    	session()->flash('message', 'submitted! Your order has been placed');

    	//redirect to home page
    	return redirect('/order');
    }
}
